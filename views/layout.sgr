doctype html
html(lang='de')
  head
    meta(charset='utf-8')
    meta(http-equiv='X-UA-Compatible' content='IE=edge')
    meta(name='description' content='In der lebenswertesten Stadt der Welt haben heuer ALLE einen Grund zum Feiern. Denn unser Ziel lautet: Gemeinsam erfüllen wir ALLE Wünsche!')
    meta(name='viewport' content='width=device-width, initial-scale=1')
    link(rel='apple-touch-icon' sizes='180x180' href='/apple-touch-icon.png?3')
    link(rel='icon' type='image/png' sizes='32x32' href='/favicon-32x32.png?3')
    link(rel='icon' type='image/png' sizes='16x16' href='/favicon-16x16.png?3')
    link(rel='manifest' href='/manifest.json?3')
    link(rel='mask-icon' href='/safari-pinned-tab.svg?3' color='#1aa7e5')
    link(rel='shortcut icon' href='/favicon.ico?3')
    meta(name='apple-mobile-web-app-title' content='Wiener Wichtel Challenge 2017')
    meta(name='application-name' content='Wiener Wichtel Challenge 2017')
    meta(name='msapplication-TileColor' content='#1aa7e5')
    meta(name='msapplication-TileImage' content='/mstile-144x144.png?3')
    meta(name='theme-color' content='#a1dbff')

    block(name='title')
      title Wiener Wichtel Challenge 2017 🎅🎄🎁

    link(rel='stylesheet' href='https://fonts.googleapis.com/css?family=Aladin|Roboto:400,400i,700,700i')
    link(rel='stylesheet' href='css/index.css?9')

  body.body(class='body--{{ pageId }}'id='body')
    block(name='content')

    footer
      p
        a#impressum-link(href='#impressum') Impressum

      div.box.box--impressum#impressum
        h2 Impressum
        p Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz
        p We Cross The Borders – Paneuropäischer Kulturverein <br>ZVR: 402903413

        h3 Organschaftliche Vertreter
        p Obmann/Obfrau: Florian Reichl <br>Obmann/Obfrau Stellvertreter: Simon Eloy
        p Diepoldplatz 4/24, A-1170 Wien <br>Tel.: +436605422151 <br>E-Mail: wecrosstheborders.info@gmail.com

        h3 Vereinszweck
        p Der Verein ist nicht auf Gewinn ausgerichtet und verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne der §§ 34 ff
        p
          ul
            li Förderung und Schaffung von Kunst und Kultur auf internationaler Ebene
            li Kultureller und künstlerischer Austausch auf internationaler Ebene
            li Bereicherung des kulturellen Lebens
            li Förderung und Mitgestaltung eines paneuropäischen Kulturraums


        h2 Haftungsausschluss
        p Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Webseite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.

        h3 Haftung für Inhalte dieser Webseite
        p Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Wir sind jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.

        h3 Haftung für Links auf Webseiten Dritter
        p Unser Angebot enthält Links zu externen Websites. Auf den Inhalt dieser externen Webseiten haben wir keinerlei Einfluss. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.

        h3 Urheberrecht
        p Die Betreiber dieser Webseite sind bemüht, stets die Urheberrechte anderer zu beachten bzw. auf selbst erstellte sowie lizenzfreie Werke zurückzugreifen. Die durch die Seitenbetreiber erstellten Inhalte und Werke auf dieser Webseite unterliegen dem Urheberrecht. Beiträge Dritter sind als solche gekennzeichnet. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.


        h2 Datenschutzerklärung
        p Wir legen großen Wert auf den Schutz Ihrer Daten. Um Sie in vollem Umfang über die Verwendung personenbezogener Daten zu informieren, bitten wir Sie die folgenden Datenschutzhinweise zur Kenntnis zu nehmen.

        h3 Persönliche Daten
        p Persönliche Daten, die Sie auf dieser Website elektronisch übermitteln, wie zum Beispiel Name, E-Mail-Adresse, Adresse oder andere persönlichen Angaben, werden von uns nur zum jeweils angegebenen Zweck verwendet, sicher verwahrt und nicht an Dritte weitergegeben. Der Provider erhebt und speichert automatisch Informationen am Webserver wie verwendeter Browser, Betriebssystem, Verweisseite, IP-Adresse, Uhrzeit des Zugriffs usw. Diese Daten können ohne Prüfung weiterer Datenquellen keinen bestimmten Personen zugeordnet werden und wir werten diese Daten auch nicht weiter aus solange keine rechtswidrige Nutzung unserer Webseite vorliegt.

        h3 Formulardaten und Kommentare
        p Wenn Webseitenbesucher Kommentare oder Formulareinträge hinterlassen, werden die eingegebenen Daten und ihre IP-Adressen gespeichert. Das erfolgt zur Sicherheit, falls jemand widerrechtliche Inhalte verfasst (Beleidigungen, links- oder rechtsextreme Propaganda, Hasspostings usw.). In diesem Fall sind wir an der Identität des Verfassers interessiert.

        h3 Cookies
        p Cookies sind kleine Dateien, die es dieser Webseite ermöglichen auf dem Computer des Besuchers spezifische, auf den Nutzer bezogene Informationen zu speichern, während unsere Website besucht wird. Cookies helfen uns dabei, die Nutzungshäufigkeit und die Anzahl der Nutzer unserer Internetseiten zu ermitteln, sowie unsere Angebote für Sie komfortabel und effizient zu gestalten. Wir verwenden einerseits Session-Cookies, die ausschließlich für die Dauer Ihrer Nutzung unserer Website zwischengespeichert werden und zum anderen permanente Cookies, um Informationen über Besucher festzuhalten, die wiederholt auf unsere Website zugreifen. Der Zweck des Einsatzes dieser Cookies besteht darin, eine optimale Benutzerführung anbieten zu können sowie Besucher wiederzuerkennen und bei wiederholter Nutzung eine möglichst attraktive Website und interessante Inhalte präsentieren zu können. Der Inhalt eines permanenten Cookies beschränkt sich auf eine Identifikationsnummer. Name, IP-Adresse usw. werden nicht gespeichert. Eine Einzelprofilbildung über Ihr Nutzungsverhalten findet nicht statt. Eine Nutzung unserer Angebote ist auch ohne Cookies möglich. Sie können in Ihrem Browser das Speichern von Cookies deaktivieren, auf bestimmte Webseiten beschränken oder Ihren Webbrowser (Chrome, IE, Firefox,…) so einstellen, dass er sie benachrichtigt, sobald ein Cookie gesendet wird. Sie können Cookies auch jederzeit von der Festplatte ihres PC löschen. Bitte beachten Sie aber, dass Sie in diesem Fall mit einer eingeschränkten Darstellung der Seite und mit einer eingeschränkten Benutzerführung rechnen müssen.

        h3 Datenschutzerklärung für die Nutzung von YouTube
        p Auf unserer Webseite sind Funktionen des Dienstes YouTube implementiert. Diese Funktionen werden durch die YouTube, LLC, 901 Cherry Ave., San Bruno, CA 94066, USA angeboten. Die eingebundenen Videos legen bei dem Aufrufen der Webseite Cookies auf den Computern der User ab. Wer das Setzen von Cookies für das Google-Werbeprogramm deaktiviert hat, wird auch beim Aufrufen von YouTube-Videos mit keinen solchen Cookies rechnen müssen. YouTube legt aber auch in anderen Cookies nicht-personenbezogene Nutzungsinformationen ab. Möchten Sie dies verhindern, müssen Sie das im Browser blockieren.

        h3 Auskunftsrecht
        p Sie haben jederzeit das Recht auf Auskunft über die bezüglich Ihrer Person gespeicherten Daten, deren Herkunft und Empfänger sowie den Zweck der Speicherung

    block(name='javascript')
      script(src='js/main.js' defer)
