function notify () {
  var body = document.getElementById('body')
  var newDiv = document.createElement('div')
  var newContent = document.createTextNode('Newsletter-Anmeldung erfolgreich!')
  newDiv.setAttribute('class', 'notification')
  newDiv.appendChild(newContent)

  document.body.insertBefore(newDiv, body.firstChild)
}

if (window.location.hash === '#newsletter-anmeldung-erfolgreich') {
  window.history.replaceState(null, document.title, window.location.pathname + window.location.search)
  notify()
}

var impressum = document.getElementById('impressum')
var impressumLink = document.getElementById('impressum-link')

impressumLink.addEventListener('click', function () {
  impressum.className += ' box--visible'
})
