# Wiener Wichtel Challenge 2017

*In der lebenswertesten Stadt der Welt haben heuer ALLE einen Grund zum Feiern. Gemeinsam erfüllen wir ALLE Wünsche!*

This repo contains the build tools and content of [www.wienerwichtelchallenge.at](http://www.wienerwichtelchallenge.at). It is built with [spike](https://spike.readme.io/), a static build tool powered by WebPack.

## Setup

- make sure [node.js](http://nodejs.org) is at version >= `6`
- `yarn global add spike`
- clone this repo and `cd` into the folder
- run `yarn`

## Development

- run `yarn run dev`

## Testing

Tests are located in `test/**` and are powered by [ava](https://github.com/sindresorhus/ava)

- `yarn` to ensure devDeps are installed
- `yarn run test` to run test suite

## Deployment

Deployment is performed with as simple bash script

- `chmod +x deploy.sh`
- `./deploy.sh`
