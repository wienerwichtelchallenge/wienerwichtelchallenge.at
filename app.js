const htmlStandards = require('reshape-standard')
const cssStandards = require('spike-css-standards')
const jsStandards = require('spike-js-standards')
const pageId = require('spike-page-id')
const sugarml = require('sugarml')
const sugarss = require('sugarss')
const env = process.env.SPIKE_ENV
const pxToRem = require('postcss-pxtorem')

module.exports = {
  devtool: 'source-map',
  matchers: { html: '*(**/)*.sgr', css: '*(**/)*.sss' },
  ignore: ['**/layout.sgr', '**/_*', '**/.*', '.**/*', 'readme.md', 'yarn.lock', 'deploy.sh'],
  outputDir: 'dist',
  reshape: htmlStandards({
    parser: sugarml,
    locals: (ctx) => { return { pageId: pageId(ctx), foo: 'bar' } },
    minify: env === 'production'
  }),
  postcss: cssStandards({
    parser: sugarss,
    minify: env === 'production',
    warnForDuplicates: env !== 'production',
    appendPlugins: [
      pxToRem({
        propList: ['*'],
        mediaQuery: true
      })
    ]
  }),
  babel: jsStandards(),
  server: {
    port: 1111,
    logLevel: 'silent',
    logPrefix: 'spike',
    notify: true,
    host: 'localhost',
    watchOptions: {
      ignored: ['node_modules']
    },
    ghostMode: false
  }
}
